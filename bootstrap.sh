# Update Packages
apt-get update
# Upgrade Packages
apt-get upgrade

# Set Apache server name
echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/fqdn.conf
a2enconf fqdn

# Basic Linux Stuff
apt-get install -y git

# Apache
apt-get install -y apache2

# Enable Apache Mods
a2enmod rewrite

#Add Onrej PPA Repo
apt-add-repository ppa:ondrej/php
apt-get update

# Install PHP
apt-get install -y php5

# PHP Apache Mod
apt-get install -y libapache2-mod-php5

# Update the apache dir.conf
sed -i '/DirectoryIndex index.html index.cgi index.pl index.php index.xhtml index.htm/ c\DirectoryIndex index.php index.html index.cgi index.pl index.xhtml index.htm' /etc/apache2/mods-enabled/dir.conf

# Restart Apache
service apache2 restart

# PHP Mods
apt-get install -y php5-common
apt-get install -y php5-mcrypt
apt-get install -y php5-zip
apt-get install -y curl
apt-get install -y php5-curl
apt-get install -y php5-cli
apt-get install -y php5-cgi
apt-get install -y php-pear
apt-get install -y php5-dev
apt-get install -y apache2-threaded-dev 
apt-get install -y php5-mongo
apt-get install -y php5-gd

update-alternatives --set php /usr/bin/php5
update-alternatives --set phar /usr/bin/phar5
update-alternatives --set phar.phar /usr/bin/phar.phar5
update-alternatives --set phpize /usr/bin/phpize5
update-alternatives --set php-config /usr/bin/php-config5

#
# PHP.ini params edits
#
sudo echo "; ######### PHP.ini modifications from vagrant bootstrap.sh #######" >> /etc/php5/apache2/php.ini
sudo echo "error_reporting = E_ALL | E_STRICT" >> /etc/php5/apache2/php.ini
sudo echo "display_errors = On" >> /etc/php5/apache2/php.ini

# Set MySQL Pass
debconf-set-selections <<< 'mysql-server mysql-server/root_password password 30mins@n00n'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password 30mins@n00n'

# Install MySQL
apt-get install -y mysql-server

# PHP-MYSQL lib
apt-get install -y php5-mysql

# Restart Apache
sudo service apache2 restart

# Install extra packages
apt-get install -y libcurl4-openssl-dev pkg-config libssl-dev

# Add MongoDB repository key
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

# Add MongoDB repository details
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list

# Update packages list
apt-get update

# Install Mongodb
apt-get install -y mongodb-org

# Install MongoDB-PHP driver
pecl install mongodb

# Add mongoDB extension to php.ini
if grep -Fxq "extension=mongodb.so" /etc/php5/apache2/php.ini
then
	sed -i '/extension=mongodb.so/ c\extension=mongodb.so' /etc/php5/apache2/php.ini
else
	echo "extension=mongodb.so" >> /etc/php5/apache2/php.ini
fi

if grep -Fxq "extension=mongodb.so" /etc/php5/cli/php.ini
then
	sed -i '/extension=mongodb.so/ c\extension=mongodb.so' /etc/php5/cli/php.ini
else
	echo "extension=mongodb.so" >> /etc/php5/cli/php.ini
fi

# Restart apache
service apache2 restart

# Start mongod
service mongod start

# Include Phalcon repository
curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash

# Update packages list
apt-get update

# Install Phalcon
apt-get -y install php5-phalcon

# Add Phalcon extension to php.ini
if grep -Fxq "extension=php_phalcon.so" /etc/php5/apache2/php.ini
then
	sed -i '/extension=php_phalcon.so/ c\extension=phalcon.so' /etc/php5/apache2/php.ini
fi

grep -q -F 'include "/extension=phalcon.so"' /etc/php5/apache2/php.ini || echo 'extension=phalcon.so' >> /etc/php5/apache2/php.ini

if grep -Fxq "extension=php_phalcon.so" /etc/php5/cli/conf.d/50-phalcon.ini
then
	sed -i '/extension=php_phalcon.so/ c\extension=phalcon.so' /etc/php5/cli/conf.d/50-phalcon.ini
fi

grep -q -F 'include "/extension=phalcon.so"' /etc/php5/cli/conf.d/50-phalcon.ini || echo 'extension=phalcon.so' >> /etc/php5/cli/conf.d/50-phalcon.ini


# Download and Install composer
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
